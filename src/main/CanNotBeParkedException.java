
public class CanNotBeParkedException extends Exception{
    public CanNotBeParkedException(String string) {
        super(string);
    }
}

