import java.util.ArrayList;

public class ParkingLot {

    public ArrayList<Object> parkedCars = new ArrayList<>();
    public final int capacity;
    public static final String PARK="Your Car is Parked";
    public static final String UNPARK="Your Car is unParked";
    public static final String NOT_PARK="The ParkingLot has reached its Full Capacity,So Your Car cannot be parked";
    public static final String CAR_NOT_PRESENT="The Car You are trying to unPark is not present in the ParkingLot";


    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public String park(Object car) throws CanNotBeParkedException {
        if (!(parkedCars.size() < capacity)) {
            throw new CanNotBeParkedException(NOT_PARK);
        }
        parkedCars.add(car);
        return PARK;
    }

    public String unPark(Object car) throws CarNotPresentException {
        if (!(parkedCars.contains(car))) {
            throw new CarNotPresentException(CAR_NOT_PRESENT);
        }
        parkedCars.remove(car);
        return UNPARK;

    }
}
