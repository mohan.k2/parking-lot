import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParkingLotTest {
    @Test
    void shouldReturnYourCarIsParkedWhenTheCarIsParked() throws CanNotBeParkedException {
        Object car = new Object();
        ParkingLot parkingLot = new ParkingLot(5);

        String actual = parkingLot.park(car);

        assertEquals(ParkingLot.PARK, actual);
    }

    @Test
    void shouldThrowAnExceptionCannotBeParkedWhenYourCarIsNotParked() throws CanNotBeParkedException {
        Object car = new Object();
        ParkingLot parkingLot = new ParkingLot(1);

        parkingLot.park(car);
        
        CanNotBeParkedException actual=assertThrows(CanNotBeParkedException.class, () -> {
            parkingLot.park(car);
        });
        assertEquals(ParkingLot.NOT_PARK,actual.getMessage());
    }

    @Test
    void shouldReturnYourCarIsUnParkedWhenTheCarIsUnParked() throws CanNotBeParkedException, CarNotPresentException {
        Object car = new Object();
        ParkingLot parkingLot = new ParkingLot(5);

        parkingLot.park(car);
        String actual = parkingLot.unPark(car);

        assertEquals(ParkingLot.UNPARK, actual);
    }

    @Test
    void shouldThrowCarNotPresentExceptionWhenTheCarToBeUnParkedIsNotPresentInTheParkingLot() throws CanNotBeParkedException {
        Object car = new Object();
        Object anotherCar = new Object();
        ParkingLot parkingLot = new ParkingLot(1);

        parkingLot.park(car);

        CarNotPresentException actual=assertThrows(CarNotPresentException.class, () -> {
            parkingLot.unPark(anotherCar);
        });

        assertEquals(ParkingLot.CAR_NOT_PRESENT,actual.getMessage());

    }


}